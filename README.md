Pelmanism, also known as Memory, is a game in which the players try to
find pairs of matching cards. In this one-player version, the goal is to
find all pairs as fast as possible.

![](http://www.rolf-sander.net/software/pelmanism/screenshot.png)

## Installation:

The following code will start pelmanism from within python:
```
import pelmanism
pelmanism.main()
```

If `$HOME/.local/bin` is included in your `$PATH`, you can start the
game directly with typing `pelmanism` in a terminal.

## Usage:

Run the script `pelmanism.py` to start the game.

## Modifications:

- To play the game with your own pictures, simply replace the `png` files
  in the cards directory by your own.
  
- To change the number of cards, adjust `Nx` and `Ny` in `pelmanism.py`.

- To change the size of the cards, adjust `thumbsize` in `pelmanism.py`.
